#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <assert.h>

#define NELEMS(X) (sizeof(X)/sizeof((X)[0]))
#define MAX_MOVES 128
#define MAX_DEPTH 512

typedef enum {
    MOVE_SPLIT,
    MOVE_MERGE,
} MoveType;

typedef struct {
    MoveType type;
    int index;
    int n;
    bool applied;
} Move;

static int * list;
static int * prior_states[MAX_MOVES];
static int prior_nitems[MAX_MOVES];
static int orig_list[] = {7, 5, 3};
static int nitems = NELEMS(orig_list);
static int napplied;
static int orig_max_item;
static int reversed_list[NELEMS(orig_list)];
static Move move_stack[MAX_DEPTH];
static int move_stack_len;

static void
init()
{
    list = malloc(sizeof(orig_list));
    memcpy(list, orig_list, sizeof(orig_list));
    for (int i = 0; i < nitems; i++) {
        if (orig_list[i] > orig_max_item)
            orig_max_item = orig_list[i];
    }
    for (int i = 0; i < nitems; i++) {
        reversed_list[i] = orig_list[nitems-i-1];
    }
}

static bool
move_is_valid(Move move)
{
    switch (move.type) {
        case MOVE_SPLIT: {
            int a = move.n;
            int b = list[move.index] - move.n;
            if (a == b)
                return false;
            for (int i = 0; i < nitems; i++) {
                if (list[i] == a || list[i] == b)
                    return false;
            }
            break;
        }
        case MOVE_MERGE: {
            assert(move.index < nitems-1);
            int sum = list[move.index] + list[move.index+1];
            if (sum > orig_max_item)
                return false;
            for (int i = 0; i < nitems; i++) {
                if (list[i] == sum)
                    return false;
            }
            break;
        }
        default: assert(0);
    }
    return true;
}

static void
record_state()
{
    assert(napplied < MAX_MOVES);
    prior_nitems[napplied] = nitems;
    prior_states[napplied] = realloc(prior_states[napplied], nitems*sizeof(list[0]));
    memcpy(prior_states[napplied], list, nitems*sizeof(list[0]));
}

static void
push_move(Move move)
{
    assert(move_stack_len < MAX_DEPTH);
    move_stack[move_stack_len++] = move;
}

static void
enumerate_moves()
{
    for (int i = 0; i < nitems; i++) {
        int m = list[i];
        for (int j = 1; j < m; j++) {
            Move move = {
                .type = MOVE_SPLIT,
                .index = i,
                .n = j,
            };
            if (move_is_valid(move))
                push_move(move);
        }
    }
    for (int i = 0; i < nitems-1; i++) {
        Move move = {
            .type = MOVE_MERGE,
            .index = i,
            .n = list[i],
        };
        if (move_is_valid(move))
            push_move(move);
    }
}

static void
_apply_top_move(bool inverse)
{
    assert(move_stack_len > 0);
    Move move = move_stack[move_stack_len-1];
    if (inverse)
        move.type = (move.type == MOVE_SPLIT) ? MOVE_MERGE : MOVE_SPLIT;
    switch (move.type) {
        case MOVE_SPLIT: {
            int a = move.n;
            int b = list[move.index] - move.n;
            list = realloc(list, sizeof(list[0])*(nitems+1));
            memmove(&list[move.index+2], &list[move.index+1], (nitems-move.index-1)*sizeof(list[0]));
            list[move.index] = a;
            list[move.index+1] = b;
            nitems++;
            break;
        }
        case MOVE_MERGE: {
            int sum = list[move.index] + list[move.index+1];
            memmove(&list[move.index+1], &list[move.index+2], (nitems-move.index-2)*sizeof(list[0]));
            list[move.index] = sum;
            nitems--;
            break;
        }
    }
    napplied += inverse ? -1 : 1;
    move_stack[move_stack_len-1].applied = !inverse;
}

#if 0
static void
print_list()
{
    for (int i = 0; i < nitems; i++) {
        printf("%d ", list[i]);
    }
    printf("\n");
}
#endif

static void
apply_top_move()
{
    _apply_top_move(false);
}

static void
undo_top_move()
{
    _apply_top_move(true);
}

static bool
list_is_reversed()
{
    if (nitems != NELEMS(orig_list))
        return false;
    return memcmp(list, reversed_list, sizeof(reversed_list)) == 0;
}

static bool
in_prior_state()
{
    for (int i = 0; i < napplied; i++) {
        if (nitems != prior_nitems[i])
            continue;
        if (memcmp(prior_states[i], list, nitems*sizeof(list[0])) == 0)
            return true;
    }
    return false;
}

static bool
top_move_is_applied()
{
    return move_stack[move_stack_len-1].applied;
}

static void
pop_move()
{
    move_stack_len--;
}

static bool
move_stack_empty()
{
    return move_stack_len == 0;
}

static bool
solve()
{
    while (1) {
        record_state();
        enumerate_moves();
        while (1) {
            apply_top_move();
            if (list_is_reversed())
                return true;
            if (!in_prior_state())
                break;
            while (top_move_is_applied()) {
                undo_top_move();
                pop_move();
                if (move_stack_empty()) {
                    return false;
                }
            }
        }
    }
}

static void
print_solution()
{
    if (move_stack_len == 0)
        printf("No solution found.\n");
    for (int i = 0, j = 0; i < move_stack_len; i++) {
        Move m = move_stack[i];
        if (m.applied) {
            printf("%c %d %d : ", m.type == MOVE_SPLIT ? 's' : 'm', m.index, m.n);
            for (int k = 0; k < prior_nitems[j]; k++) {
                printf("%d ", prior_states[j][k]);
            }
            printf("\n");
            j++;
        }
    }
}

int main()
{
    init();
    solve();
    print_solution();
}
